class HashMapCustom<K, V> {
     
    private Entry<K,V>[] table;   //Entryk halmaza
    private int capacity= 4;  //Mekkora legyen a hashmap
    
    static class Entry<K, V> {
        K key;
        V value;
        Entry<K,V> next;
    
        public Entry(K key, V value, Entry<K,V> next){
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }
    

   @SuppressWarnings("unchecked")
   public HashMapCustom(){
      table = new Entry[capacity];
   }

    /**
    * @param newKey
    * @param data
    */
   public void put(K newKey, V data){
      if(newKey==null)
          return;    
     
      //úúj kulcs hash értéke
      int hash=hash(newKey);
      //új entryt hozunk létre
      Entry<K,V> newEntry = new Entry<K,V>(newKey, data, null);
     
      //ha az adott hely nem tartalmaz elemet, akkor ide rakjuk a fent létrehozott entryt
       if(table[hash] == null){
        table[hash] = newEntry;
       }else{
          Entry<K,V> previous = null;
          Entry<K,V> current = table[hash];
          
          while(current != null){ //elértük az utolsó tárhelyünket
          if(current.key.equals(newKey)){           
              if(previous==null){ 
                    newEntry.next=current.next;
                    table[hash]=newEntry;
                    return;
              }
              else{
                  newEntry.next=current.next;
                  previous.next=newEntry;
                  return;
              }
          }
          previous=current;
            current = current.next;
        }
        previous.next = newEntry;
       }
   }

   /**
    * Kulcs értékének kiiratása
    * @param key
    */
   public V get(K key){
       int hash = hash(key);
       if(table[hash] == null){
        return null;
       }else{
        Entry<K,V> temp = table[hash];
        while(temp!= null){
            if(temp.key.equals(key))
                return temp.value;
            temp = temp.next; //visszaadja a kulcs értékét
        }         
        return null;   //null értéket ad vissza ha nem találja a kulcsot
       }
   }
 

   /**
    * Jelenítsük meg a tárolt elemeinket
    * @param key
    */
   public void display(){
      
      for(int i=0;i<capacity;i++){
          if(table[i]!=null){
                 Entry<K, V> entry=table[i];
                 while(entry!=null){
                       System.out.print("{"+entry.key+"="+entry.value+"}" +" ");
                       entry=entry.next;
                 }
          }
      }             
   
   }

}