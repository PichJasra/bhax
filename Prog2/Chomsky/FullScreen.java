package fullscreen;

import java.awt.*;
import javax.swing.*;
public class FullScreen {
   public static void main(String[] args) {
      GraphicsEnvironment graphics =
      GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice device = graphics.getDefaultScreenDevice();
      JFrame frame = new JFrame("Fullscreen");
      JPanel panel = new JPanel();
      JLabel label = new JLabel("", JLabel.CENTER);
      label.setText("Ez egy teljes képernyős app");
      label.setOpaque(true);
      frame.add(panel);
      frame.add(label);
      frame.setUndecorated(true);
      frame.setResizable(false);
      device.setFullScreenWindow(frame);
   }
}
