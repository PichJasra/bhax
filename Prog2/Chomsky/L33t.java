package l33t;

import java.util.Scanner;
import java.util.regex.Pattern;

public class L33t {
    public static void main(String[] args) {
    String leet[] = {"4", "8", "(", ")", "3", "}", "6", "#", "!", "]", "X", "|", "M", "N", "0", "9", "Q", "2", "Z", "7", "M", "V", "W", "X", "J", "Z"};
    String result = "";

    Scanner sc = new Scanner(System.in);
    String Str = sc.nextLine().toUpperCase(); 
    for (int i = 0; i < Str.length(); ++i){
        result +=leet[Str.charAt(i) - 'A'];
    }
    System.out.println(result);
}
}
