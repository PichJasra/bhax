package liskov;
public class Liskov {
    public static class Program {
        public static void fgv(Man man) {
            man.jatszik
        }
    }

    public static class Ember {
        void jatszik() {};
    }

    public static class Gyerek extends Ember{}

    public static class Felnott extends Ember {}

    public static void main(String[] args) {
        Ember ember = new Ember();
        Program.fgv(ember);

        Gyerek gyerek = new Gyerek();
        Program.fgv(gyerek);

        Felnott felnott = new Felnott();
        Program.fgv(felnott);
    }
}