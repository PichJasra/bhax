A feladat egy olyan program írása volt, amely hogyha nem követi a Yoda conditions
feltételeit
akkor egy NullPointerException-nal kilép. A NullPointerException egy RuntimeException
vagyis a hiba csak futtatás után fog jelentkezni. Ezt unchecked kivételnek nevezzük
ellenben a checked kivételekkel melyekre már futtatás előtt felhívja figyelmünket a fordító.
NullPointerException-t leggyakrabban akkor kapunk, hogyha megpróbálunk egy olyan
metódust vagy változót futtatni, vagy esetünkben elérni aminek referenciája a null értékre
“mutat”, vagy olyan helyen próbálunk meg null értéket használni ahol a program egy “igazi”
értéket várna.
Erre példa a YodaConditions programban megfigyelhető. Itt is az equals() metódus egy igazi
értéket várna, így mikor egy null értéket tartalmazó változót adunk meg a program
NullPointerExceptionnel kilép futás közben.