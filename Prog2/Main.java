package iris;

import java.io.*;
import java.io.FileNotFoundException; 
import java.util.Scanner;
import com.opencsv.CSVReader;

public class Main {

}
	class ReadCSVData { 
	    private static final String CSV_FILE_PATH 
	        = "/home/eclipse-workspace/Iris/iris.csv"; 
	  
	    public static void main(String[] args) 
	    { 
	  
	        System.out.println("Read Data Line by Line With Header \n"); 
	        readDataLineByLine(CSV_FILE_PATH); 
	        System.out.println("_______________________________________________"); 
	    }
	  
	    public static void readDataLineByLine(String file) 
	    { 
	  
	        try { 
	  
	            // Create an object of filereader class 
	            // with CSV file as a parameter. 
	            FileReader filereader = new FileReader(file); 
	  
	            // create csvReader object passing 
	            // filereader as parameter 
	            CSVReader csvReader = new CSVReader(filereader); 
	            String[] nextRecord; 
	  
	            // we are going to read data line by line 
	            while ((nextRecord = csvReader.readNext()) != null) { 
	                for (String cell : nextRecord) { 
	                    System.out.print(cell + "\t"); 
	                } 
	                System.out.println(); 
	            } 
	        } 
	        catch (Exception e) { 
	            e.printStackTrace(); 
	        } 
	    } 
}
